// Providers
provider "aws" {
  region              = var.aws_default_region
  profile             = var.aws_profile
  allowed_account_ids = [var.aws_account_id]

  assume_role {
    role_arn = "arn:aws:iam::${var.aws_account_id}:role/${var.aws_role_name}"
  }
}
