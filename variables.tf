/**
 * Generic Variables
 */

variable "project" {
  type        = string
  description = "Organization name"
}

variable "environment" {
  type        = string
  description = "Environment name"
}


/**
 * AWS Variables
 */

variable "aws_default_region" {
  type        = string
  description = "Default region to deploy to on AWS"
  default     = "us-east-1"
}

variable "aws_profile" {
  type        = string
  description = "AWS credentials profile to use"
  default     = "default"
}

variable "aws_account_id" {
  type        = string
  description = "AWS account ID to use"
}

variable "aws_role_name" {
  type        = string
  description = "AWS Role Name to use"
  default     = "OrganizationAccountAccessRole"
}

/**
 * Local Variables
 */

locals {
  common_tags = {
    Terraform   = "true"
    Automation  = "true"
    Environment = var.environment
  }
}
