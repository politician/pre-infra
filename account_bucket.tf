/**
 * Bucket used before spinning up or destroying infrastructure
 */

resource "aws_s3_bucket" "account_files" {
  bucket        = "${var.project}-${var.environment}-account-files"
  acl           = "private"
  force_destroy = true

  tags = merge(local.common_tags, {
    Name = "${title(var.environment)} Account Files"
  })

  lifecycle {
    // This is a safety measure to make sure this bucket is not destroyed accidentally along with all account backups (switch to false to allow destroy)
    prevent_destroy = true
  }
}

resource "aws_s3_bucket_public_access_block" "account_files" {
  bucket = aws_s3_bucket.account_files.id

  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}

resource "aws_iam_policy" "account_files_policy" {
  name   = "Access${title(var.environment)}AccountFilesBucketPolicy"
  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "s3:Get*",
                "s3:Put*",
                "s3:List*",
                "s3:Delete*"
            ],
            "Resource": [
                "arn:aws:s3:::${aws_s3_bucket.account_files.id}/*",
                "arn:aws:s3:::${aws_s3_bucket.account_files.id}"
            ]
        }
    ]
}
EOF
}

resource "aws_iam_role" "account_files_role" {
  name = "Access${title(var.environment)}AccountFilesBucket"
  tags = local.common_tags

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": [
          "ec2.amazonaws.com"
        ]
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "account_files_role_policy" {
  role       = aws_iam_role.account_files_role.name
  policy_arn = aws_iam_policy.account_files_policy.arn
}

/*
 * Create group with sync access
 */

// Create group
resource "aws_iam_group" "sync_group" {
  name = "Access${title(var.environment)}AccountFilesBucket"
}

// Attach policy
resource "aws_iam_group_policy_attachment" "sync_group_policy" {
  group      = aws_iam_group.sync_group.id
  policy_arn = aws_iam_policy.account_files_policy.id
}

/*
 * Create sync user
 */

// Create user
resource "aws_iam_user" "sync_user" {
  name          = "${var.environment}_sync_account_files"
  force_destroy = true
  tags          = local.common_tags
}

// Attach group
resource "aws_iam_user_group_membership" "sync_user_group" {
  user   = aws_iam_user.sync_user.name
  groups = [aws_iam_group.sync_group.name]
}

// Create programmatic access
resource "aws_iam_access_key" "sync_user" {
  user = aws_iam_user.sync_user.name
}

/**
 * Outputs
 */

output "bucket_name" {
  value       = aws_s3_bucket.account_files.id
  description = "Account files bucket name"
}

output "sync_user" {
  value = {
    name                  = aws_iam_user.sync_user.id,
    aws_access_key_id     = aws_iam_access_key.sync_user.id
    aws_secret_access_key = aws_iam_access_key.sync_user.secret
  }
  description = "Sync user with access to Account Files bucket"
}
