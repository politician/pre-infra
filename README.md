# Pre-Infrastructure

This repository will create a separately managed S3 bucket to store persisted data even when the infrastructure is destroyed (simply because it is managed separately).
Run it in as many accounts as you need environments.

## Prerequisite

- [Terraform](https://www.terraform.io/downloads.html)

> If you use homebrew on Mac OSX, you can just run `brew install terraform`

## Configure

### Variables

[Set the variables](https://www.terraform.io/docs/configuration/variables.html#assigning-values-to-root-module-variables) accordingly.

For example:

```sh
cat << EOF >> terraform.tfvars
/**
 * Generic Variables
 */

project     = "my-project"
environment = "dev"


/**
 * AWS Variables
 */

aws_default_region = "us-east-1"
aws_account_id     = "10010101001" // Dev environment
EOF
```

> If you are using [AWS CLI](https://aws.amazon.com/cli/) to manage your AWS credentials, you may have different profiles and should set the variable `aws_profile`, otherwise leave it empty and it will default to the `[default]` profile.

### Backend

If you want to use a remote backend, just create a file `backend.tf`

For example:

```sh
cat << EOF >> backend.tf
terraform {
  backend "remote" {
    hostname     = "app.terraform.io"
    organization = "my-org"
    workspaces {
      name = "pre-infra-dev"
    }
  }
}
EOF
```

## Usage

1. Initialize terraform and its modules

   ```sh
   terraform init
   ```

2. Test your plan

   ```sh
   terraform plan
   ```

3. Apply your plan (deploy the infrastructure)

   ```sh
   terraform apply
   ```
